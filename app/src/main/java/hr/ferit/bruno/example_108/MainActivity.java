package hr.ferit.bruno.example_108;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    private static final java.lang.String KEY_FILE_PATH = "filepath";
    @BindView(R.id.ibTakePicture) ImageButton ibTakePicture;
    @BindView(R.id.ivShotTaken) ImageView ivShotTaken;
    private static final int CODE_TAKE_PICTURE = 10;
    String mFilePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if(savedInstanceState != null){
            if(savedInstanceState.containsKey(KEY_FILE_PATH)){
                this.mFilePath = savedInstanceState.getString(KEY_FILE_PATH);
                loadImage();
            }
        }
    }

    @OnClick(R.id.ibTakePicture)
    public void takePicture(){
        Intent takePic = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePic.resolveActivity(getPackageManager()) != null){
            startActivityForResult(takePic, CODE_TAKE_PICTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CODE_TAKE_PICTURE){
            if(resultCode == RESULT_OK){
                Uri pictureUri = data.getData();
                this.mFilePath = getPathFromUri(pictureUri);
                this.loadImage();
            }
        }
    }

    private String getPathFromUri(Uri pictureUri) {
        Cursor cursor  = null;
        String data = MediaStore.Images.Media.DATA;

        String[] projection = new String[]{data};
        cursor = this.getContentResolver().query(pictureUri,projection,null,null,null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(data));
        return path;
    }

    private void loadImage() {
        File file = new File(this.mFilePath);
        Picasso.with(this)
                .load(file)
                .fit()
                .centerCrop()
                .into(ivShotTaken);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if(this.mFilePath != null){
            outState.putString(KEY_FILE_PATH, this.mFilePath);
        }
    }
}
